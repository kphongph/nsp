const raw = require('raw-socket')

const header = Buffer.alloc(12)

const checksum = (buff) => {
  const buffer = Buffer.from(buff)
  var sum = 0
  for(var i=0;i<buffer.length;i=i+2) {
    sum += buffer.readUIntLE(i,2)
  }
  sum = (sum >> 16) + (sum & 0xFFFF)
  sum += (sum >> 16)
  sum = ~sum
  return (new Uint16Array([sum]))[0]
}

header.writeUInt8(0x8,0)
header.writeUInt16LE(1023,4)
header.writeUInt16LE(checksum(header),2)

const socket = raw.createSocket({
  protocol: raw.Protocol.ICMP
})

const target = '8.8.8.8'

socket.send(header,0,12,target,(err,bytes) => {
  console.log ("sent " + bytes + " bytes to " + target)
})

socket.on('message',(buffer,source) => {
  console.log ("received " + buffer.length + " bytes from " + source);
	console.log ("data: " + buffer.toString ("hex"));
  socket.close()
})
