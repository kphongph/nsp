const raw = require('raw-socket')

const checksum = (buff) => {
  const buffer = Buffer.from(buff)
  var sum = 0
  for(var i=0;i<buffer.length;i=i+2) {
    sum += buffer.readUIntLE(i,2)
  }
  sum = (sum >> 16) + (sum & 0xFFFF)
  sum += (sum >> 16)
  sum = ~sum
  return (new Uint16Array([sum]))[0]
}

const header = Buffer.alloc(12)
header.writeUInt8(0x8,0)
header.writeUInt16LE(1000,4)
header.writeUInt16LE(1000,6)
header.writeUInt16LE(1000,8)
header.writeUInt16LE(checksum(header),2)

const socket = raw.createSocket({ protocol: raw.Protocol.ICMP })

socket.on('error',e => { 
  console.log('Socket Error:',e)
  socket.close()
})

const target = '8.8.8.8'

socket.send(header,0,12,target,(err,bytes) => {
	console.log ("data: " + header.toString ("hex"));
  console.log ("sent " + bytes + " bytes to " + target)
})

const decodePingPacket = (buffer) => {
  const versionAndIHL = buffer.readUInt8()
  const IHL = versionAndIHL & 0b00001111
  const ipOffset = IHL * 4
  console.log(`Version = ${(versionAndIHL & 0b11110000) >> 4}`)
  console.log(`IHL = ${(versionAndIHL & 0b00001111)}`)
  const srcIP = [12,13,14,15].map(item => buffer.readUInt8(item)).join('.')
  const desIP = [16,17,18,19].map(item => buffer.readUInt8(item)).join('.')
  console.log(`Source IP = ${srcIP}`)
  console.log(`Destination IP = ${desIP}`)
  const ttl = buffer.readUInt8(8)
  console.log(`Time To Live = ${ttl}`)
  const type = buffer.readUInt8(ipOffset)
  const code = buffer.readUInt8(ipOffset+1)
  const checksum = buffer.readUInt16BE(ipOffset + 2)
  const identifier = buffer.readUInt16BE(ipOffset + 4)
  const seqNumber = buffer.readUInt16BE(ipOffset + 6)
  const secondsPart = buffer.readUInt32BE(ipOffset + 8)
  //const microseconds = buffer.readUInt32BE(ipOffset + 12)
  return { ttl, type, code, checksum, identifier, seqNumber, secondsPart }
}

socket.on('message',(buffer,source) => {
  console.log ("received " + buffer.length + " bytes from " + source);
	console.log ("data: " + buffer.toString ("hex"));
  const pck = decodePingPacket(buffer)
  /*
  socket.send(header,0,12,target,(err,bytes) => { 
    console.log ("sent " + bytes + " bytes to " + target)
  })
  */
  socket.close()
})
