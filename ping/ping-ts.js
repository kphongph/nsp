const raw = require('raw-socket')

const checksum = (buff) => {
  const buffer = Buffer.from(buff)
  var sum = 0
  for(var i=0;i<buffer.length;i=i+2) {
    sum += buffer.readUIntLE(i,2)
  }
  sum = (sum >> 16) + (sum & 0xFFFF)
  sum += (sum >> 16)
  sum = ~sum
  return (new Uint16Array([sum]))[0]
}

const now = Date.now()
console.log("timestamp: ", now)
console.log("timestamp: ", `0${now.toString(16)}`)
console.log("timestamp: ", Buffer.from(`0${now.toString(16)}`,'hex'))

const header = Buffer.alloc(10)
header.writeUInt8(0x8,0)
//header.writeUInt16LE(1023,4)
header.fill(Buffer.from(`0${now.toString(16)}`,'hex'),4)
header.writeUInt16LE(checksum(header),2)

const socket = raw.createSocket({ protocol: raw.Protocol.ICMP })

socket.on('error',e => { 
  console.log('Socket Error:',e)
  socket.close()
})

const target = '8.8.8.8'

socket.send(header,0,10,target,(err,bytes) => {
  console.log ("sent " + bytes + " bytes to " + target)
})

const decodePingPacket = (buffer) => {
  const versionAndIHL = buffer.readUInt8()
  const IHL = versionAndIHL & 0b00001111
  const ipOffset = IHL * 4
  const srcIP = [12,13,14,15].map(item => buffer.readUInt8(item)).join('.')
  const desIP = [16,17,18,19].map(item => buffer.readUInt8(item)).join('.')
  const ttl = buffer.readUInt8(8)
  console.log(`Time To Live = ${ttl}`)
  const type = buffer.readUInt8(ipOffset)
  const code = buffer.readUInt8(ipOffset+1)
  const checksum = buffer.readUInt16BE(ipOffset + 2)
  // get ts
  const ts_array = Buffer.from(buffer).slice(ipOffset + 4,ipOffset + 10)
  const ts_int = parseInt(ts_array.toString('hex'), 16)
  // diff 
  const now = Date.now()
  const diff = now - ts_int
  console.log(`current -> ${now}`)
  console.log(`packet  -> ${ts_int}`)
  console.log(`diff    -> ${now-ts_int}ms`)
  return { ttl, type, code, checksum }
}

socket.on('message',(buffer,source) => {
  console.log ("received " + buffer.length + " bytes from " + source);
	console.log ("data: " + buffer.toString ("hex"));
  decodePingPacket(buffer)
  socket.close()
})
